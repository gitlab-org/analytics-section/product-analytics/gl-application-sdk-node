# GitLab Application SDK - Node

This SDK is for using GitLab Application Services with Node.

## How to use the SDK

### Using the NPM package

Add the NPM package to your package JSON using your preferred package manager:

```shell
yarn add @gitlab/application-sdk-node
```

OR

```
npm i @gitlab/application-sdk-node
```

### Initialize SDK

```js
import { GitLabSDK } from '@gitlab/application-sdk-node';

const glSDK = new GitLabSDK({ appId: 'YOUR_APP_ID', host: 'YOUR_HOST' });
```

## SDK initialization options

| Option   | Description                                                                                                            |
| :------- | :--------------------------------------------------------------------------------------------------------------------- |
| `appId` | The ID specified in the GitLab Project Analytics setup guide. It ensures your data is sent to your analytics instance. |
| `host`   | The GitLab Project Analytics instance specified in the setup guide.                                                    |

## Methods

### `identify`

Used to associate a user and their attributes with the session and tracking events.

```js
glSDK.identify('123abc', { user_name: 'Matthew' });
```

| Property          | Type     | Description                                                              |
| :---------------- | :------- | :----------------------------------------------------------------------- |
| `user_id`         | `String` | The ID of the user.                                                      |
| `user_attributes` | `Hash`   | Optional. The user attributes to add to the session and tracking events. |

### `track`

Used to trigger a custom event.

```js
glSDK.track(event_name, event_attributes);
```

| Property           | Type     | Description                                       |
| :----------------- | :------- | :------------------------------------------------ |
| `event_name`       | `String` | The name of the custom event.                     |
| `event_attributes` | `Hash`   | The event attributes to add to the tracked event. |

## Developing with the devkit

To develop with a local Snowplow pipeline, use Analytics devkit's [Snowplow setup](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit/-/tree/main#setup).

## Example Node App
`examples/server-app` is a sample application based on Express that uses this Node SDK.

To run the sample application:
1. Install nodejs 22.2.0
```bash
# using asdf
asdf install nodejs 22.2.0
```
2. Set PA_COLLECTOR_URL & PA_APPLICATION_ID environment variables
```bash
export PA_COLLECTOR_URL=<events_collector_url> # e.g. http://localhost:9091 if running using devkit
export PA_APPLICATION_ID=<application_id> # product analytics application id for gitlab project
```
3. Build SDK and start the app
```bash
# from root folder of this repository
yarn install
yarn build
cd examples/server-app
yarn install
node server.mjs
```
4. Check that app is accessible at `http://localhost:5173`
5. Send event
```bash
curl http://localhost:5173/api/v1/send_event`
```
or open in browser `http://localhost:5173/api/v1/send_event`

## Contribute

Want to contribute to the GitLab Application SDK - Node? follow [contributing guide](https://gitlab.com/gitlab-org/analytics-section/product-analytics/gl-application-sdk-node/-/blob/main/CONTRIBUTING.md).
