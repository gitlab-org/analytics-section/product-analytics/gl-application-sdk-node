const { GitLabSDK } = require('../../lib/index.cjs');

const glSDK = new GitLabSDK({ host: 'http://localhost:9090', appId: '123' });

glSDK.identify('123', {
  userRandomProps: 'Value',
});

glSDK.track('custom_event', {
  key1: 'value1',
});
