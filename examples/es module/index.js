import { GitLabSDK } from '../../lib/index.mjs';

const APPLICATION_ID = '123';
const HOST = 'http://localhost:9090';

const glSDK = new GitLabSDK({ host: HOST, appId: APPLICATION_ID });

glSDK.identify('123', {
  userRandomProps: 'Value',
});

glSDK.track('custom_event', {
  key1: 'value1',
});
