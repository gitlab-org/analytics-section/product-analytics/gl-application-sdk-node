import express from "express";
import { GitLabSDK } from '../../lib/index.mjs';

const app = express();
const port = 5173;
const APPLICATION_ID = process.env.PA_APPLICATION_ID;
const COLLECTOR_HOST = process.env.PA_COLLECTOR_URL;
const glSDK = new GitLabSDK({ host: COLLECTOR_HOST, appId: APPLICATION_ID });

app.get('/', (req, res) => {
  res.send('Express server!');
});

app.get('/api/v1/send_event', (req, res) => {
  if (!COLLECTOR_HOST || !APPLICATION_ID) {
    throw new Error('Environment variables PA_COLLECTOR_URL & PA_APPLICATION_ID must be set!');
  }

  glSDK.identify('123', {
    userRandomProps: 'Value',
  });
  
  glSDK.track('custom_event', {
    id: '123',
  });
  
  res.send('Event sent!');
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
