import {
  tracker,
  gotEmitter,
  buildSelfDescribingEvent,
  SelfDescribingEvent,
  SelfDescribingJson,
} from '@snowplow/node-tracker';
import { GitLabSDKOptions } from '../src/types';
import { DEFAULT_TRACKER_ID, SCHEMAS } from '../src/utils/constants';
import { GitLabSDK } from '../src';
import currentUser from '../src/utils/currentUser';

// Mock dependencies
jest.mock('@snowplow/node-tracker', () => ({
  ...jest.requireActual('@snowplow/node-tracker'),
  gotEmitter: jest.fn(),
  tracker: jest.fn().mockImplementation(() => ({
    track: jest.fn(),
    setUserId: jest.fn(),
    setSessionIndex: jest.fn(),
  })),
  buildSelfDescribingEvent: jest.fn(),
}));
jest.mock('../src/utils/currentUser', () => ({
  userId: 'testUserId',
  userAttributes: { attr1: 'value1', attr2: 'value2' },
}));

describe('GitLabSDK', () => {
  let sdkOptions: GitLabSDKOptions;
  let sdk: GitLabSDK;

  beforeEach(() => {
    sdkOptions = {
      appId: 'testAppId',
      host: 'http://localhost:9090',
      trackerId: DEFAULT_TRACKER_ID,
    };
    sdk = new GitLabSDK(sdkOptions);

    currentUser.userId = 'testUserId';
    currentUser.userAttributes = { attr1: 'value1', attr2: 'value2' };
  });

  describe('constructor', () => {
    it('should initialize tracker', () => {
      expect(gotEmitter).toHaveBeenCalledWith(
        'localhost',
        'http',
        9090,
        'post',
        1,
        5
      );
      expect(tracker).toHaveBeenCalled();
    });

    it('should log warning and return when no appId is provided', () => {
      console.warn = jest.fn();
      sdkOptions.appId = '';
      new GitLabSDK(sdkOptions);
      expect(console.warn).toHaveBeenCalledWith(
        'GitLab: No appId was provided'
      );
    });

    it('should log warning and return when no host is provided', () => {
      console.warn = jest.fn();
      sdkOptions.host = '';
      new GitLabSDK(sdkOptions);
      expect(console.warn).toHaveBeenCalledWith('GitLab: No host was provided');
    });
  });

  describe('identify', () => {
    it('should set userId and userAttributes', () => {
      const userId = 'newUserId';
      const userAttributes = { newAttr: 'newValue' };
      sdk.identify(userId, userAttributes);
      expect(currentUser.userId).toBe(userId);
      expect(currentUser.userAttributes).toEqual(userAttributes);
    });
    it('should set userId when userAttributes are not passed', () => {
      const userId = 'newUserId';
      sdk.identify(userId);
      expect(currentUser.userId).toBe(userId);
      expect(currentUser.userAttributes).toEqual({});
    });
  });

  describe('track', () => {
    it('should call tracker.track with correct parameters', () => {
      const eventName = 'testEvent';
      const eventAttributes = { attr1: 'value1', attr2: 'value2' };

      const expectedEvent: SelfDescribingEvent = {
        event: {
          schema: SCHEMAS.CUSTOM_EVENT,
          data: {
            name: eventName,
            props: eventAttributes,
          },
        },
      };

      const expectedUserContext: SelfDescribingJson = {
        schema: SCHEMAS.USER_CONTEXT,
        data: currentUser.userAttributes,
      };

      sdk.track(eventName, eventAttributes);

      expect(sdk.tracker?.track).toHaveBeenCalledWith(
        buildSelfDescribingEvent(expectedEvent),
        [expectedUserContext]
      );
    });
    it('should call tracker.track with correct parameters when eventAttributes are not passed', () => {
      const expectedEvent: SelfDescribingEvent = {
        event: {
          schema: SCHEMAS.CUSTOM_EVENT,
          data: {
            name: 'testEvent',
            props: {},
          },
        },
      };

      const expectedUserContext: SelfDescribingJson = {
        schema: SCHEMAS.USER_CONTEXT,
        data: currentUser.userAttributes,
      };

      sdk.track('testEvent');

      expect(sdk.tracker?.track).toHaveBeenCalledWith(
        buildSelfDescribingEvent(expectedEvent),
        [expectedUserContext]
      );
    });

    it('should not call setUserId when userId is null', () => {
      currentUser.userId = null;
      sdk.track('testEvent', {});
      expect(sdk.tracker?.setUserId).not.toHaveBeenCalled();
    });

    it('should not include userContext in tracker.track call when userAttributes is empty', () => {
      currentUser.userAttributes = {};
      const eventName = 'testEvent';
      const eventAttributes = { attr1: 'value1', attr2: 'value2' };
      const expectedEvent: SelfDescribingEvent = {
        event: {
          schema: SCHEMAS.CUSTOM_EVENT,
          data: {
            name: eventName,
            props: eventAttributes,
          },
        },
      };
      sdk.track(eventName, eventAttributes);
      expect(sdk.tracker?.track).toHaveBeenCalledWith(
        buildSelfDescribingEvent(expectedEvent),
        []
      );
    });
  });
});
