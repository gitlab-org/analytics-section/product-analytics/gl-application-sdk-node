import { parseUrl } from '../../src/utils/parseUrl';

describe('parseUrl', () => {
  it('should correctly parse a URL with http', () => {
    const url = 'http://localhost:8000';
    const result = parseUrl(url);
    expect(result).toEqual({
      protocol: 'http',
      hostname: 'localhost',
      port: 8000,
    });
  });

  it('should correctly parse a URL with https', () => {
    const url = 'https://localhost:8000';
    const result = parseUrl(url);
    expect(result).toEqual({
      protocol: 'https',
      hostname: 'localhost',
      port: 8000,
    });
  });

  it('should return default ports if none specified', () => {
    const httpUrl = 'http://localhost';
    const httpsUrl = 'https://localhost';
    const httpResult = parseUrl(httpUrl);
    const httpsResult = parseUrl(httpsUrl);
    expect(httpResult).toEqual({
      protocol: 'http',
      hostname: 'localhost',
      port: 80,
    });
    expect(httpsResult).toEqual({
      protocol: 'https',
      hostname: 'localhost',
      port: 443,
    });
  });

  it('should return null for an invalid URL', () => {
    const url = 'not a valid URL';
    const result = parseUrl(url);
    expect(result).toBeNull();
  });
});
