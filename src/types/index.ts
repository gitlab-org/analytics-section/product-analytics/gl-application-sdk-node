export interface GitLabSDKOptions {
  /** The app id to send with each event */
  appId: string;
  /** Collector endpoint in the form collector.mysite.com */
  host: string;
  /** The tracker id - also known as tracker namespace */
  trackerId: string;
}

/**
 * Gitlab SDK instance
 */
export type GitLabSDKType = {
  /** Track the user defined custom event */
  track: (eventName: string, eventAttributes?: Record<string, any>) => void;
  /** Set the business defined userId and other user attributes */
  identify: (userId: string, userAttributes?: Record<string, any>) => void;
};

export type CurrentUserType = {
  userId: string | null;
  userAttributes: Record<string, any>;
};
