import {
  tracker,
  gotEmitter,
  Tracker,
  buildSelfDescribingEvent,
  SelfDescribingEvent,
  SelfDescribingJson,
  HttpMethod,
  HttpProtocol,
} from '@snowplow/node-tracker';
import { GitLabSDKOptions } from './types';
import { DEFAULT_TRACKER_ID, SCHEMAS } from './utils/constants';
import { isEmpty } from './utils/isEmpty';
import { parseUrl } from './utils/parseUrl';
import currentUser from './utils/currentUser';

export class GitLabSDK {
  options: GitLabSDKOptions = {
    appId: '',
    host: '',
    trackerId: DEFAULT_TRACKER_ID,
  };
  tracker: Tracker | null = null;
  constructor(options: GitLabSDKOptions) {
    this.options = { ...this.options, ...options };
    const { appId, host, trackerId } = this.options;

    if (!appId) {
      console.warn('GitLab: No appId was provided');
      return;
    }

    if (!host) {
      console.warn('GitLab: No host was provided');
      return;
    }

    const parsedUrl = parseUrl(host);
    if (!parsedUrl) {
      console.warn('GitLab: Invalid host URL');
      return;
    }
    const { protocol = HttpProtocol.HTTP, hostname, port } = parsedUrl;
    const emitter = gotEmitter(hostname, protocol, port, HttpMethod.POST, 1, 5);

    this.tracker = tracker([emitter], trackerId, appId, false);
    this.tracker.setSessionIndex(1);
  }

  /**
   * Track the user defined custom event
   *
   * @param {eventName} eventName -  Name of the custom event
   * @param {eventAttributes} eventAttributes -  Additional information for custom event
   * @returns {void}
   */
  public track(
    eventName: string,
    { ...eventAttributes }: Record<string, any> = ({} = {})
  ): void {
    const SelfDescribingJson: SelfDescribingEvent = {
      event: {
        schema: SCHEMAS.CUSTOM_EVENT,
        data: {
          name: eventName,
          props: { ...eventAttributes },
        },
      },
    };

    this.setUserId();
    const userContext = this.setUserContext();
    this.tracker?.track(
      buildSelfDescribingEvent(SelfDescribingJson),
      userContext ? [userContext] : []
    );
  }

  /**
   * Set the user ID
   */
  private setUserId(): void {
    const { userId } = currentUser;

    if (userId) {
      this.tracker?.setUserId(userId);
    }
  }

  /**
   * Set the user context
   */
  private setUserContext() {
    const { userAttributes } = currentUser;

    if (isEmpty(userAttributes)) {
      return;
    }

    const userContext: SelfDescribingJson = {
      schema: SCHEMAS.USER_CONTEXT,
      data: userAttributes,
    };

    return userContext;
  }

  /**
   * Set the business defined userId and other user attributes
   *
   * @param {userId} userId -  The business-defined user ID
   * @param {userAttributes} userAttributes -  The business-defined user attributes
   * @returns {void}
   */
  public identify(
    userId: string,
    userAttributes: Record<string, any> = ({} = {})
  ): void {
    currentUser.userId = userId;
    currentUser.userAttributes = userAttributes;
  }
}
