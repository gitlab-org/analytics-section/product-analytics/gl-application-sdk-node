export const SCHEMAS = Object.freeze({
  CUSTOM_EVENT: 'iglu:com.gitlab/custom_event/jsonschema/1-0-0',
  USER_CONTEXT: 'iglu:com.gitlab/user_context/jsonschema/1-0-0',
});

export const DEFAULT_TRACKER_ID = 'gitlab';
