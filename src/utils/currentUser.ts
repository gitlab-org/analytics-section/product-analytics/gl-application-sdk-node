import { CurrentUserType } from '../types';

let currentUser: CurrentUserType = {
  userId: null,
  userAttributes: {},
};

export default currentUser;
