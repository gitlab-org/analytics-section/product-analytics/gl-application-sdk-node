import { HttpProtocol } from '@snowplow/node-tracker';

export function parseUrl(url: string) {
  try {
    const parsedUrl = new URL(url);
    return {
      protocol:
        parsedUrl.protocol.replace(':', '') === HttpProtocol.HTTP
          ? HttpProtocol.HTTP
          : HttpProtocol.HTTPS, // remove ':' from 'https:' or 'http:'
      hostname: parsedUrl.hostname,
      port: parsedUrl.port
        ? parseInt(parsedUrl.port)
        : parsedUrl.protocol === 'https:'
        ? 443
        : 80,
    };
  } catch (error) {
    console.error(`Invalid URL: ${url}`);
    return null;
  }
}
